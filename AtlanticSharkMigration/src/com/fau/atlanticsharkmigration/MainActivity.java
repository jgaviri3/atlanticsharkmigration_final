package com.fau.atlanticsharkmigration;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	// variables
	Button report, id, map;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// liking xml menu buttons to java
		report = (Button) findViewById(R.id.bReport);
		id = (Button) findViewById(R.id.bshark_id);
		map = (Button) findViewById(R.id.bMap);

		// setting the listener for any of the buttons in the menu
		report.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//whe this button is press the ReportActivity opens
				// TODO Auto-generated method stub
				Intent intent = new Intent(v.getContext(), ReportActivity.class);
				startActivityForResult(intent,0);

			}
		});

		id.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// opening IdActivity
				Intent intent = new Intent(v.getContext(), IdActivity.class);
				startActivityForResult(intent,0);

			}
		});

		map.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// opening MapActivity
				
				
                   // Log.d("Firstdroid.Gps", "Loading Map..");
                    // Loading Google Map View
                   // startService(new Intent(MainActivity.this, MapActivity.class));
				     
				
				
				
				Intent intent = new Intent(v.getContext(), MapActivity.class);
				startActivityForResult(intent,0);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
