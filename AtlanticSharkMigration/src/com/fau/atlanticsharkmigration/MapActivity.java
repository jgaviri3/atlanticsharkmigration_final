package com.fau.atlanticsharkmigration;


import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MapActivity extends FragmentActivity


        implements
        ConnectionCallbacks,
        OnConnectionFailedListener,
        LocationListener
        		         {

    private GoogleMap mMap;

    private LocationClient mLocationClient;
    private TextView mMessageView;
    

    // These settings are the same as the settings for the map. They will in fact give you updates
    // at the maximal rates currently possible.
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    
    
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mMessageView = (TextView) findViewById(R.id.message_text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        setUpLocationClientIfNeeded();
        CurrentLocation();
       
       
        DBAccess dba = new DBAccess();
        
        List<DBAccess.sharkPosition> sharkPositionList = dba.GetSpinner();
    	//String returnMessage = new String();
    	                
    	for(int i=0; i < sharkPositionList.size(); i++)
    	{
    		DBAccess.sharkPosition sp = sharkPositionList.get(i);
    	    //returnMessage += sp.latitude + ":" + sp.longitude + ":" + sp.sharkType + System.getProperty("line.separator");
    	    
    	    LatLng pos = new LatLng(Float.parseFloat(sp.latitude),Float.parseFloat(sp.longitude));
    		
    		mMap.addMarker(new MarkerOptions()
    				.title("Spinner")
    				.snippet(sp.comment)
    				.position(pos)
    				.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)
    				)
    				
    				);
    	}
    	
        List<DBAccess.sharkPosition> BlackTipPositionList = dba.GetBlackTip();
    	//String returnMessage = new String();
    	                
    	for(int i=0; i < BlackTipPositionList.size(); i++)
    	{
    		DBAccess.sharkPosition sp = BlackTipPositionList.get(i);
    	    //returnMessage += sp.latitude + ":" + sp.longitude + ":" + sp.sharkType + System.getProperty("line.separator");
    	    
    	    LatLng pos = new LatLng(Float.parseFloat(sp.latitude),Float.parseFloat(sp.longitude));
    		
    		mMap.addMarker(new MarkerOptions()
    				.title(sp.title)
    				.snippet(sp.comment)
    				.position(pos)
    				.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)
    				)
    				
    				);
    	}
    	
        mLocationClient.connect();
        
    }

    
    
    
    private void CurrentLocation() {
    	CameraUpdate center=
    	        CameraUpdateFactory.newLatLng(new LatLng(30.218570,
    	        		-74.926758));
    	    CameraUpdate zoom=CameraUpdateFactory.zoomTo(5);

    	    mMap.moveCamera(center);
    	    mMap.animateCamera(zoom);
	}

    
    
    
	private void AddMarkers(List<DBAccess.sharkPosition> markerList)
    {
    	List<DBAccess.sharkPosition> sharkPositionList = markerList;
    	//String returnMessage = new String();
    	                
    	for(int i=0; i < sharkPositionList.size(); i++)
    	{
    		DBAccess.sharkPosition sp = sharkPositionList.get(i);
    	    //returnMessage += sp.latitude + ":" + sp.longitude + ":" + sp.sharkType + System.getProperty("line.separator");
    	    
    	    LatLng pos = new LatLng(Float.parseFloat(sp.latitude),Float.parseFloat(sp.longitude));
    		
    		mMap.addMarker(new MarkerOptions()
    				.title(sp.title)
    				.snippet(sp.comment)
    				.position(pos)
    				.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
    				)
    				
    				);
    	}
    	
    }
		

	@Override
    public void onPause() {
        super.onPause();
        if (mLocationClient != null) {
            mLocationClient.disconnect();
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
              
              
            }
        }
    }

    private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }

    /**
     * Button to get current Location. This demonstrates how to get the current Location as required
     * without needing to register a LocationListener.
     */
    public void showMyLocation(View view) {
        if (mLocationClient != null && mLocationClient.isConnected()) {
            String msg = "Location = " + mLocationClient.getLastLocation();
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }
    
    	
		/**
     * Implementation of {@link LocationListener}.
     */
    @Override
    public void onLocationChanged(Location location) {
        mMessageView.setText("Location = " + location);
    }

    /**
     * Callback called when connected to GCore. Implementation of {@link ConnectionCallbacks}.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener
    }

    /**
     * Callback called when disconnected from GCore. Implementation of {@link ConnectionCallbacks}.
     */
    @Override
    public void onDisconnected() {
        // Do nothing
    }

    /**
     * Implementation of {@link OnConnectionFailedListener}.
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Do nothing
    }

   
}
